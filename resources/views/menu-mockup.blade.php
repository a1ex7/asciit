<div class="header">
    <img id="BSheaderLogo" src="http://academy.binary-studio.com/resources/logo.png" class="logo"/>
    <div>
        <div class="buttons">
            <input id="searchBar" placeholder="Search"/>
            <button class="searchBtn">Filter</button>
            <a id="userLink" class="noTextDecoration">
                <div id="userProfile">
                    <img id="avatar" class="avatar" src="http://team.binary-studio.com/profile/api/files/get/31859551-3951-4d3e-83ab-2ce58c361c1b.jpg">
                    <span id="userName">Edik Dolynskyi</span>
                </div>
            </a>
            <button id="appsBtn">
                <img src="http://team.binary-studio.com/app/images/Thumbnails.png" class="appsLogo"/>
            </button>
            <button id="notificationBtn">
                <img src="http://team.binary-studio.com/app/images/bell.png" class="appsLogo"/>
            </button>
        </div>
        <div id="search"></div>
    </div>
</div>
<div id="logOutBox" class="invisible">
    <div id="userprofileBtnInBox" class="logOutButtons">
        <button id="userProfileInBoxBtn" class="userprofileAndLogoutBtn">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAADXElEQVRoQ+2ai3HTQBCGow7oAKcCQgU4FQAVxK6ApAJMBSQVoFRAUgGmgiQVoFQAqcD8v2elWZ3Puj3lLqcx7IzGr729/bS3q3u4OjoQqQ6E42gQZLPZzAD6CdcHXHxfQhp0eoPrqqoqvveKFwQAr6D9FdeihOcDfdb47QJAf1ydHRCB+AHFk4lBtO7c482pC9MDCUA8wsDe0GaC5nB+7bG9A+OCMHRnTsNbfD4fGp+ZILZmJU8v8fa90881fOqGfgciDX45yksoE664wD86/c1x5Li9wRqE1KxQrdxCidVqMgIYVi8dGVayczqoQRiNmfK6ox1Dgk7fqXYPvkoTaxc2WYDuVLsGdo9dkI1SeISChjL1KcWCUeVdYgnXUuPDl+fmGvpoYKcrALC3DYaOiAb5id/nJu9FSXLsOz4OlW3W/wUuvvJm0akoQT9rNOiinQOEIY999tRwZBlDkhUExldw5nOMQ0qXw43tTZINRPKChcLNCZNjomQuLDlBOObd+h4DQV3On1j+g5ITxK3tQWc8CuZnVk4Q9/kzBsRcJXOC6LI9BmLbpi2jIQP/QQx3KEVEnhARU9XLGZEUyT6JHOG8isvi50hvbTFkKGdE3On/GKBJlN81PNdT9jEgkxhaBwOSYmhNIkfcVduYoXWK8svIBiVbsrNnGK/x4u6+BJ0SBXN+SF+9oZxjYTUGhntlJzHr+awRkTvFHRcudWPEPH1vjWYHEZgGr76dQR/cE76cxUTjRYaWdLLAq3WRFbXEfdGICIxl7hWdGyVAVug0tBERnRslQHgkMQ9k/BK5wSoXLVmTXXZSON9ijlj2i7lBx1kznyEsEGZJDiLO8yFI52M35rTjBGFecQa8DhElA5GtUeYAAVILoVYAut5nOBak2+XWBmGEAKvU3nvsEYh5tBMh+PAbv3XLYt8UhY31w+wtlHjExbkUG5Y4V7yEDxctqOdYgccV22E9dNBzA6WPBSFa/7uNbvjCKZAuJt6DnhmU3KM3DiMepIyd2aYahe2OffjoTYZQPQGnrfC9xZjveJoJ9sZqrZDeA/qd6wnnvj8MTBlmB6KX7PrOSoJzLV46N9yAX+ELPmc4M+iJ5U81nEqwUljXGalHG2fKfPKzFDf7jP8bf3NKfWtz2vsLW+k7UaxgsbwAAAAASUVORK5CYII=" class="userprofileAndLogoutImg"/>
        </button>
    </div>
    <div id="logOutBtnInBox" class="logOutButtons">
        <button id="logOutButton" class="userprofileAndLogoutBtn">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAB+0lEQVRoQ+2ajU3DMBCFkw26AWECugEdoUxA2KCdgDIBMAHpBnQDmICOEDZgg/Ce5KDIOPFVtiM7sqWoVeOf+3zPd47TslhIKRfCUWSQ2Dwp9kjXdRsYf4+rmgmixTjHsiw/JOOJQACxQ2fPkg4D1NkD5sXWrxUEEGt08mXrKPD9a8DQQ6NFAnJA68fAhtq6fwII7XACoUZvBz0c8b2xjex4v0Z7rse+nACy9Q1inR1HiAJy1lXwCZBNBuEMYHZ0aWWPSCWXpTU1U1laUh0Z6qkkzBB/o26nGbVUkFnhk1sT5pM0QNRerkGu+NEdhHtMjtskEqJah1cw+A4Gnw0wKxPksJ5krxU8j2gBZQejXy9dYjGCkOEd14PNC7F7pLevHZOayVuxemRoq0hqQUGg/e5SrY/Ut0otFRDyTUotJZBvlU/+hWdSpgJygq31VBQLCiJdH4aN6bCpt1OUuRNiDzEpJX2SYvWIVUopgIikFDNINRaVsIaS2TTySDb9bbwputEL+J3nzTWuNB6sdBD1qPuG33nuzJIsyAHGD8+bM8ift/NxkHR/YqiXTxqnJm/J0mqYvByUI2nK3MGrL15e9OihUGKI7zrWVxmS3e8yXoZyahfxerrXiPrDAHVb+dbNSH8t16LXPwzMZLjTMNY14tT7jI0zyIyTLRrqF74MkEI5G3CLAAAAAElFTkSuQmCC" class="userprofileAndLogoutImg"/>
        </button>
    </div>
</div>
<div id="appsBlock" class="invisible">
    <div id="appsList"></div>
</div>
<div id="notificationBlock" class="invisible">
    <ul id="notificationList"></ul>
    <a href="#" class="noTextDecoration specialNotifBtn">
        <img style="width:20px;height:20px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABKklEQVRIS82WwQ2CMBiFKQvoBuoGjqCbYAKc3UCdwDuQWCfREWQD3IAzB/D9xDZQCC0pREl6adr3/f/rawpzZv7YzPqOBPi+v3Nd9wbg2hKalWV5SJLkSToSEARBxhhbWYqL7VkURZsWIAzDaiLxWgaAunjZwb8DUhTLMa7ChSk7SIui2HHOc7hwBuA0pUVSHCHZQviBoCynAjTFPQhTxOVna9GguEkHFyw6Yix6oqsV1wEuaO9MnqJtuo1NiJH4IKCqqhwL9nEcvxTIHWk5Ulow3/Fc7XTwDFQINnsAkmWOibjOorqYJkRUZypuBFAhY8SNAQKCw+bfVPWEqn/K9h5oQT8BUDz7Lpa2WnUBAvJG6tY033oyyWvbV43EMbzOkzm6TMMNs/9VfAAM8dYZ68JZ8wAAAABJRU5ErkJggg=="/></a><a href="http://team.binary-studio.com/app/#/" class="noTextDecoration specialNotifBtn"><img width="20" height="20" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAABjklEQVRIS+2VTVICMRCFk7Eg2ckNxIXgTjyBcAM9gXgTbiKeQG8g3gB3ggvHG4y7BMppX1MyQpnJj7jTbGZqql++dPebtBSJa6bUFBLqWnuaIpUpwRw705r42TUmSRsdPNd6WApxCUGfQaBN8D4G8CbmsEHQixCthVL3Qsqec0OiadPawaEQhQ8YBK16IuWJ99SAhXrmBXG5UKLryNJcdYwZ18V6QVHZrHcOZOUHfTosJqOQE/9B3ir6fuI/VLpZs3kusuw21nGruLK86C4Wdy6Ns3TzRqNHWcbXTisJRFTIshx0lku+4bfWN9CPIV8/rhO2BdoZ4oFVoF+D1MAq0JPWPF/OknoSCOaZdWzMgMMqUNIFGnkagB4AWg3KCsQDzio1woch3LYfuZc7jOgNkLGydrQeiE57P2vdx9jmk7QhaEuiVu3wI3okKQtslCM+z1CuI2MmQXvvlIlHHBzlm1oYJofggL8h0xz1R2XiVhIIhinW/QPoFaB2HGbDDDEC7t07msyxezCNqxd1+3wAMG2yG2hk98oAAAAASUVORK5CYII="/></a><a href="http://team.binary-studio.com/app/#/settings" class="noTextDecoration specialNotifBtn"><img width="20" height="20" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAACO0lEQVRIS61W3VHbQBDe1RUAHWCeExMJ7GdMBYEKAh3IFQAV+DoAKoipAPFsM1ZQMpNH0QF5ZUZedi1LPsk6cRpyM37weW+//fm+XSM4noNZeAWIl6U50fXzUF+5PEcXI7Hpz8YRIhwX9gRwnwwmpy7vt0C+LELfW4KfHOlb08HBfJzy9z0jk5gzCUyb/lN4vvQg/hPo2LyvgAiAyuABAHcJaKoUXMSBfs3vcVGPOlMUiEN/Ee5mGdwgIGdGr5mCExOoBDEBNs4o5bKk/HhkKwsHE7GTHgfGn7KYFaASpD8PT9nZT5cau9gw+Fky0FOxrZSrPx/f8sUPFydtNpz9HZPivLCpgEhtlxlK0zYN7o744inypZeNIHLJ2cSM/K277/yF9IjLdPIBu7ZZlD+GeyLSv4c6+joLR4gYcjDfm4J5U7T/N9BpmYkomVhk2wwxuGIRXl2gVcA1MxEiZJFxkO2n0EPdSjLyEFlX7ccJ5HkwsY4flyCdQD6fyaon6HPCPRurZMQwY87qRWnrCffgF9unSCRs3RzbjFpTcwoe6uRw8pizCy5t42aLXV2i+6jBq2AIHpPhZGTVSa544GmLPReHzTaUegoCq+JZ7VObwLqA1nu4mcK8cJDwpouzNltCuigWX22fYMQPd4zHL1zj1Fy7dcfSA/5dymsO1X9M+1GxuJrYtQKSca0Uhe6bEfV6TVQAJKjmHZ9Br1g4ReT1HS864J0h+iqPLL6lgrR1x7fV+L/+W7EBfeZ/1zupeig6i56mpgAAAABJRU5ErkJggg=="/>
    </a>
</div>